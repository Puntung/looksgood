import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { Root } from 'native-base'
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import configureStore from './redux/configureStore';
import AppNavigator from './navigation/AppNavigator';
const store = configureStore()
export default class App extends React.Component {
  state = {
    loadFont: false
  }
  async componentDidMount() {
    await Font.loadAsync({
      kanit: require('./assets/fonts/Kanit-Regular.ttf'),
      kanitB: require('./assets/fonts/Kanit-Bold.ttf')
    }).then(() => this.setState({
      loadFont: true
    }))
  }

  render() {
    if (this.state.loadFont) {
      return (
        <Provider store={store}>
          <View style={styles.container}>
            <Root>
              {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
              <AppNavigator />
            </Root>
          </View>
        </Provider>
      );
    }
    else {
      return (<AppLoading />)
    }

  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
