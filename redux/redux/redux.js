import {LOADING} from '../actions/actionType'
const initialState = {
    loading:false
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOADING:
            return {
                ...state,
                loading:action.loading
            }
        default:
            return state;
    }
}

export default reducer;