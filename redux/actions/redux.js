import { LOADING } from './actionType'
export const loadingState = (loading) => {
    return {
        type: LOADING,
        loading
    }
}