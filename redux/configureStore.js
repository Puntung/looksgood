import { combineReducers, createStore } from 'redux'
import reducer from './redux/redux'
const rootReducer = combineReducers({
    redux:reducer
})

const configureStore = () => {
    return createStore(
        rootReducer
    )
}

export default configureStore;