import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
  TextInput,
  FlatList,
  Dimensions,
  Animated
} from 'react-native';
import { connect } from 'react-redux';
import socketIOClient from 'socket.io-client';
import { Icon } from 'native-base'
import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'
import { Video } from 'expo-av'
import { loadingState } from '../redux/actions/index'
import axios from '../constants/axios'
const { width, height } = Dimensions.get('screen')
class HomeScreen extends React.Component {
  constructor() {
    super()

    this.state = {
      input: '',
      message: [],
      endpoint: "http://server-chat-api.herokuapp.com/", // เชื่อมต่อไปยัง url ของ realtime server
      fadeAnim: new Animated.Value(0),
      widthBar1: new Animated.Value(0),
      widthBar2: new Animated.Value(80),
      buttonFade: new Animated.Value(0),
      borderCircle: new Animated.Value(0),
      borderFade: new Animated.Value(1),
      location:""
    }
  }
  response = () => {
    const { endpoint, message } = this.state
    const temp = message
    const socket = socketIOClient(endpoint)
    socket.on('new-message', (messageNew) => {
      temp.push(messageNew)
      this.setState({ message: temp })
    })
  }
  async componentDidMount() {
    // this.response()
    await Animated.sequence([
      Animated.parallel([
        Animated.timing(
          this.state.fadeAnim,
          {
            toValue: 1,
            duration: 2000
          }
        ),
        Animated.timing(
          this.state.widthBar1,
          {
            toValue: width,
            duration: 2000
          }
        ),
        Animated.timing(
          this.state.widthBar2,
          {
            toValue: width,
            duration: 2000
          }
        )
      ]),
      Animated.timing(
        this.state.buttonFade,
        {
          toValue: 1,
          duration: 1000
        }
      ),
      Animated.loop(
        Animated.parallel([
          Animated.timing(
            this.state.borderCircle,
            {
              toValue: 1,
              duration: 2000,
            }
          )
          ,
          Animated.timing(
            this.state.borderFade,
            {
              toValue: 0,
              duration:3000
            }
          )
        ])
        
      )
      
    ]).start()

  }
  searchRes = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION)
    if (status != 'granted') {
      return;
    }
    let location = await Location.getCurrentPositionAsync({})
    
    let radius = '500'
    var url = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${location.coords.latitude},${location.coords.longitude}&radius=${radius}&type=restaurant&key=AIzaSyC2KS_Zpxe9XF1Tow_CQdC1idaeho3imaw`
    var data = await axios.axiosWithoutBody(url, 'get')
    console.log('Store =>', data.results)
  }
  headerComp = () => {
    return (
      <View>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <TextInput value={this.state.input} placeholder="MESSAGE HERE" onChangeText={(text) => this.setState({ input: text })} style={{ fontSize: 20 }} />
          <TouchableOpacity onPress={() => this.send()}>
            <Icon type="FontAwesome" name="paper-plane" style={{ fontSize: 20 }} />
          </TouchableOpacity>
        </View>
      </View>
    )

  }
  send = () => {
    const { endpoint, input } = this.state
    const socket = socketIOClient(endpoint)
    socket.emit('sent-message', input)
    this.setState({ input: '' })
  }
  render() {

    return (
      <View style={styles.container}>
        <Video resizeMode="cover" style={styles.video} source={require('../assets/video/bgcolor.mp4')} isMuted={true} isLooping shouldPlay />
        <View style={styles.bgOp}></View>
        <View style={{ flex: 1, alignItems: 'center', zIndex: 3 }}>
          <View style={{ marginTop: height / 8 }}>
            <Animated.Text style={{ fontFamily: 'kanit', fontSize: 30, color: '#273043', opacity: this.state.fadeAnim }}>ค้นหาร้านรอบๆบริเวณนี้</Animated.Text>
          </View>
          <Animated.View style={{ opacity: this.state.buttonFade,alignItems:'center',justifyContent:'center',marginTop:100}}>
            <Animated.View style={{position:'absolute',width: 300, height: 300, borderRadius: 300 / 2,borderWidth:1,borderColor:'#F34213',opacity:this.state.borderFade,transform:[{scale:this.state.borderCircle}]}}>

            </Animated.View>
            <TouchableOpacity onPress={()=>this.searchRes()} style={{ width: 160, height: 160, borderRadius: 160 / 2, backgroundColor: '#FF516C' ,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white', fontFamily: 'kanit', fontSize: 20 }}>ค้นหา</Text>
              <Icon name="search" type="FontAwesome" style={{fontSize:30,marginTop:10,color:'white'}}/>
            </TouchableOpacity>
          </Animated.View>
          <View style={{ width, position: 'absolute', bottom: 0 }}>
            <Animated.View style={{ width: this.state.widthBar1, height: 30, backgroundColor: '#FFCFD2' }}></Animated.View>
            <Animated.View style={{ width: this.state.widthBar2, height: 30, backgroundColor: '#FFA5AB',justifyContent:'center',alignItems:'center' }}>
              <Animated.Text style={{fontFamily:'kanit',fontSize:10,color:'white',opacity:this.state.buttonFade}}>Development © by Pondnkrite 2020 </Animated.Text>
            </Animated.View>
          </View>
        </View>
      </View>
    );
  }

}

HomeScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: StatusBar.currentHeight,
    position: 'relative'
  },
  bgOp: {
    width: width,
    height: height,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    zIndex: 2,
    backgroundColor: 'white',
    opacity: 0.9
  },
  video: {
    width: width,
    height: height,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
  },
});

const mapStateToProps = state => {
  return {
    loading:state.redux.loading
  }
}
const mapDispatchToProps = dispatch => {
  return {
    loadingState:(status) => dispatch(loadingState(status))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen)