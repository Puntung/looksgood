import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
  TextInput,
  FlatList,
  Dimensions,
  Animated,
  ActivityIndicator
} from 'react-native';
const { width, height } = Dimensions.get('screen')

export default class Loading extends React.Component{
  render() {
    return (
      <View style={{width,height,zIndex:5,opacity:0.5}}>

      </View>
    )
  }
}