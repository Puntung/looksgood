import axios from 'react-native-axios';
export default {
    async axiosWithBody(url, type,body) {
        try {
            let response = await axios({
                method: type,
                url: url,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                data:body
            }).then((response) => response)
            return response
        }
        catch (e) {
            return e
        }
    },
    async axiosWithoutBody(url, type) {
        try {
            let response = await axios({
                method: type,
                url: url,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then((response) => response)
            return response
        }
        catch (e) {
            return e
        }
    }
}